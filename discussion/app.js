//  CREATE A STANDARD SERVER USING NODE

// 1. Identify the ingredients/components needed in order to perform the task

// http -> will allow Node JS to establish a connection in order to transfer data using the Hyper Text Transter Protocol.
//  require() -> this will allow us to acquire the resurces needed to perform the task
let http = require('http');

// 2. Create a server using methods that we willl take from the http resources
 // the http moduule contains the createServer() method that will allow us to create a staandard server set-up

// 3. indentify the procedure/task that the server will perform once the connection has been established
	// identify the communication between the client and the server
http.createServer( function(request, response) {
	response.end('Server is up and running');
}).listen(3000)

// 4. Select an address/locatioln where the connection willl be established or hosted.

// 5. Create a responce in the terminal to make sure if the server is running successfully.
console.log('Server is running successfully');

// 6. Initialize a node package manager (NPM) in our project.

// 7. Install your first package/dependency from the NPM that will alllow you to make crtain tasks a lot more easier
	// Adjacent Skills
	// > Install
	// > Uninstall
	// > Multiple Installations
		// Syntax 1: npm install <list of dependencies>
		// Syntax 2: npm i <list of dependencies>
	// 1. nodemon
	// 2. express


// 8. Create a remote reository to backup the project.
	// Note: the node_modules should not included in the files structure that will be saved online

	// 1. Its going to take a longer time to stage/push you projects online 
	// 2. the node_modules will cause error upon deployment

	// USE .gitignore module. => will allow us to modify the components/contents taht version control will track